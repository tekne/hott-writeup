\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Overview}{2}{section.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}Introduction and Goals}{2}{subsection.1.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}Basic Design and Implementation}{2}{subsection.1.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3}Previous Attempts and Inspiration}{3}{subsection.1.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.4}Next Steps}{3}{subsection.1.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Details and Current Status}{5}{section.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Syntax}{5}{subsection.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.1}Basic Examples}{5}{subsubsection.2.1.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.2}Inductive Definitions and Builtins}{5}{subsubsection.2.1.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}API and Internal Representation}{7}{subsection.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.1}Nets}{7}{subsubsection.2.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.2}Node Kinds}{8}{subsubsection.2.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.3}Judgemental Equality}{9}{subsubsection.2.2.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.4}Typing}{10}{subsubsection.2.2.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Evaluation}{10}{subsection.2.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4}Positivity}{10}{subsection.2.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5}Type Inference}{11}{subsection.2.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.6}Memoizing Equality Checks}{11}{subsection.2.6}% 
